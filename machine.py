# Viola & Johns

import cv2
cascadeClassifierPath = "haarcascades/haarcascade_frontalface_alt2.xml"
cascadeClassifier = cv2.CascadeClassifier(cascadeClassifierPath)

# imgPath = "59619d01479a45d5008b45da.jpg"
# img = cv2.imread(imgPath)
# grayImage = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# detectedFaces = cascadeClassifier.detectMultiScale(grayImage,scaleFactor = 1.1, minSize = (20,20))
# for (x,y,w,h) in detectedFaces:
#cv2.rectangle(img , (x,y) , (x+w,y+h) , (0,255,0) , 5 )
# cv2.imwrite("res.jpeg" , img)

# Pour une video

# cap = cv2.VideoCapture(0)
# while(cap.isOpened()):
#     _,frame = cap.read()
#     grayImage = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#     detectedFaces = cascadeClassifier.detectMultiScale(grayImage , scaleFactor = 1.1, minNeighbors = 10 , minSize = (20,20))
#     for (x,y,w,h) in detectedFaces:
#         cv2.rectangle(frame , (x,y) , (x+w,y+h) , (0,255,0) , 5 )
#     cv2.imshow("res" , frame)
#     if(cv2.waitKey(1) == ord('q')):
#         break;
# cap.release()
# cv2.destroyAllWindows()